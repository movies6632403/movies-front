import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isModalVisible: false,
        mode: null,
        movieId: null,
    },
    mutations: {
        setModalVisibility(modal, isVisible) {
            modal.isModalVisible = isVisible;
        },
        setModalMode(modal, newMode) {
            modal.mode = newMode;
        },
        setMovieId(modal, id) {
            modal.movieId = id;
        },
    }
});
