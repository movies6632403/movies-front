import Vue from 'vue'
import App from './App.vue'
import store from './store';
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import axios from 'axios';
import VueToastr  from 'vue-toastr';

axios.defaults.baseURL = process.env.VUE_APP_API_URL;

Vue.prototype.$http = axios;
Vue.use(VueToastr);



import EditButton from '@/components/EditButton.vue';
import SynchronizeButton from '@/components/SynchronizeButton.vue';
import AddButton from '@/components/AddButton.vue';
import DeleteButton from '@/components/DeleteButton.vue';
import ModalTemplate from '@/components/ModalTemplate.vue';

Vue.config.productionTip = false

Vue.component('EditButton', EditButton);
Vue.component('SynchronizeButton', SynchronizeButton);
Vue.component('AddButton', AddButton);
Vue.component('DeleteButton', DeleteButton);
Vue.component('ModalTemplate', ModalTemplate);

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
